const debug = require("debug")("ikman:scraper");
const EventEmitter = require("events");
const url = require("proper-url-join");
const queryString = require("querystring");
const cheerio = require("cheerio");
const Sugar = require("sugar");
const http = require("./utils/request");

const DOMAIN = "https://ikman.lk";

class Ikman extends EventEmitter {
  constructor(config = {}) {
    super();
    // Utilities
    this._createRequest = http;

    // URL Parameters
    const defaultsForURL = {
      language: "en",
      region: undefined,
      category: undefined
    };
    const optionsForURL = { ...defaultsForURL, ...config.query };
    this._searchURL = this._createSearchURL(optionsForURL);

    // Crawler Parameters
    this._maxNumberOfPages =
      config.crawler && config.crawler.maxNumberOfPages
        ? config.crawler.maxNumberOfPages
        : undefined;
    // Instance Parameters
    this._currentPageNumber = 0;
  }

  async scrape() {
    let noMorePages = false;
    let currentPageURL = this._searchURL;
    let allListings = [];

    try {
      while (!noMorePages) {
        debug("Getting page", currentPageURL);
        let listingsForCurrentPage = await this._getListingsFromPage(
          currentPageURL
        );

        this._currentPageNumber++;
        debug("Increment", this._currentPageNumber);

        let listings = listingsForCurrentPage.listings;
        allListings.push(...listings);
        this.emit("data", listings);
        debug("Pushing to stream");

        const isLimited =
          this._maxNumberOfPages &&
          this._maxNumberOfPages === this._currentPageNumber;

        const isLastPage =
          listingsForCurrentPage && !listingsForCurrentPage.hasNext;

        console.log("isLastPage", isLastPage);
        console.log("isLimited", isLimited);

        if (isLastPage || isLimited) {
          debug("Last Page or reached maxNumberOfPages");
          this.emit("end");
          noMorePages = !noMorePages;
        } else {
          currentPageURL = listingsForCurrentPage.hasNext;
        }
      }
      return Promise.resolve(allListings);
    } catch (exception) {
      console.log("error!", exception);
      throw exception;
    }
  }

  _createSearchURL(options) {
    debug("[_createURL] Creating URL with options:", options);
    let baseURL = url(DOMAIN, options.language, "ads");
    if (options.region) {
      baseURL = url(baseURL, options.region);
    }
    if (options.category) {
      baseURL = url(baseURL, options.category);
    }
    baseURL = url(
      baseURL,
      `?${queryString.stringify({
        query: options.searchTerm
      })}`
    );
    debug("[_createURL] Created URL:", baseURL);
    return baseURL;
  }

  async _getListingsFromPage(pageURL) {
    try {
      debug("[getListingsFromPage] Getting body of: ", pageURL);
      let response = await this._createRequest(pageURL);
      let $ = cheerio.load(response);

      let listings = [];
      // jQuery Magic
      $(".ui-item").each((index, element) => {
        const titleElement = $(element).find("a.item-title.h4");
        const title = titleElement.length > 0 ? titleElement.text() : null;

        const link = url(DOMAIN, titleElement.attr("href"));

        const dateElement = $(element).find("div.item-date");
        const dateText =
          dateElement.length > 0
            ? dateElement
                .text()
                .replace(/min(s)?/gi, "minutes ago")
                .replace(/hr(s)?/gi, "hours ago")
                .replace(/day(s)?/gi, "days ago")
            : null;
        const date = dateText ? Sugar.Date.create(dateText) : null;

        let priceElement = $(element)
          .find(".item-content")
          .find(".item-info");
        let price =
          priceElement.length > 0
            ? Number(priceElement.text().replace(/[^0-9\.]+/g, ""))
            : null;

        let locationElement = $(element)
          .find(".item-content")
          .find(".item-location")
          .find(".item-area");
        let location =
          locationElement.length > 0 ? locationElement.text() : null;

        let categoryElement = $(element)
          .find(".item-content")
          .find(".item-location")
          .find(".item-cat");
        let category =
          categoryElement.length > 0 ? categoryElement.text() : null;

        let isMemberElement = $(element)
          .find(".item-content")
          .find(".item-location")
          .find(".is-member");
        let isMember = isMemberElement.length > 0 ? true : false;

        let isPromotedElement = $(element)
          .find(".item-extras")
          .find(".item-promotion");

        let isPromoted = isPromotedElement.length > 0 ? true : false;

        let thumbnailElement = $(element)
          .find(".item-thumb")
          .find("a")
          .children("img");

        let thumbnail =
          thumbnailElement.length > 0
            ? thumbnailElement
                .attr("data-srcset")
                // Remove whitespace
                .replace(/\s+/g, "")
                // Replace // with https://
                .replace(/\/\//gi, "https://")
                // Remove attribs for srcset
                .replace(/(\d\.)?(\dx)/gi, "")
                .split(",")
            : null;
        let listing = {
          title,
          price,
          link,
          date,
          location,
          category,
          isMember,
          isPromoted,
          thumbnail
        };
        listings.push(listing);
      });

      // Get details for listings
      let listingPromise = listings.map(async singleListing => {
        let details = await this._getDetailsForListing(singleListing.link);
        singleListing.details = details;
        return singleListing;
      });
      let listingsWithDetails = await Promise.all(listingPromise);

      // Get the URL for the next page, if available.
      let hasNextElement = $("a.col-6.lg-3.pag-next");
      let hasNext =
        hasNextElement.length > 0
          ? url(DOMAIN, hasNextElement.attr("href"))
          : false;

      return Promise.resolve({
        listings: listingsWithDetails,
        hasNext: hasNext
      });
    } catch (exception) {
      return Promise.reject(exception);
    }
  }

  async _getDetailsForListing(pageURL) {
    try {
      let response = await this._createRequest(pageURL);
      let $ = cheerio.load(response);
      let isDeactivated =
        $("div.ui-panel.is-rounded.item-deactivated").length > 0 ? true : false;
      if (!isDeactivated) {
        let title = $("div.row.lg-g div.item-top.col-12.lg-8 h1").text();
        let location = $("span.location").text();
        let description = $("div.col-12.lg-8 div.item-description").html();
        let isMember = $("span.poster span.ui-bubble.is-member").length === 1;
        let date = Sugar.Date.create($(" p.item-intro span.date").text());
        let contactNumbers = [];
        $("ul li.clearfix span").each((index, element) => {
          contactNumbers.push($(element).text());
        });
        let images = [];
        $("div.gallery-item").each((index, element) => {
          let image = $(element)
            .children("img")
            .attr("data-srcset");
          if (image) {
            image = image
              //Remove whitespace
              .replace(/\s+/g, "")
              // Replace // with https://
              .replace(/\/\//gi, "https://")
              // Remove attribs for srcset
              .replace(/(\d\.)?(\dx)/gi, "")
              .split(",");
            images.push(image);
          }
        });

        let seller = null;
        if (isMember) {
          seller = {
            name: $("p.item-intro span.poster a").text(),
            url: url(DOMAIN, $("p.item-intro span.poster a").attr("href"))
          };
        } else {
          seller = { name: $("p.item-intro span.poster").text() };
        }
        let attributes = {};
        $("div.item-properties dl").each((index, element) => {
          let attribute = $(element)
            .find("dt")
            .text()
            .toLowerCase()
            .replace(":", "");
          let value = $(element)
            .find("dd")
            .text()
            .toLowerCase();
          attributes[attribute] = value;
        });
        return Promise.resolve({
          title,
          location,
          seller,
          images,
          contactNumbers,
          date,
          description,
          attributes,
          isMember
        });
      } else {
        return Promise.resolve({
          isDeactivated: true
        });
      }
    } catch (exception) {
      debug("error", exception);
      return Promise.reject(exception);
    }
  }
}

module.exports = Ikman;
