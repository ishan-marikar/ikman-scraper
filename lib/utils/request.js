const axios = require("axios");
const axiosCloudflare = require("axios-cloudflare");
const retryAxios = require("retry-axios");
const userAgent = require("random-user-agent");
const debug = require("debug")("ikman:request");

const scheduleRequests = (axiosInstance, intervalMs) => {
  let lastInvocationTime = undefined;

  const scheduler = config => {
    const now = Date.now();
    if (lastInvocationTime) {
      lastInvocationTime += intervalMs;
      const waitPeriodForThisRequest = lastInvocationTime - now;
      if (waitPeriodForThisRequest > 0) {
        return new Promise(resolve => {
          setTimeout(() => resolve(config), waitPeriodForThisRequest);
        });
      }
    }

    lastInvocationTime = now;
    return config;
  };

  axiosInstance.interceptors.request.use(scheduler);
};

retryAxios.attach();
axiosCloudflare(axios);
scheduleRequests(axios, 100);

const createRequest = async url => {
  axios.defaults.headers.common["User-Agent"] = userAgent("desktop");
  try {
    let response = await axios({
      method: "GET",
      url: url,
      raxConfig: {
        retryDelay: 300,
        retry: Number.MAX_SAFE_INTEGER,
        noResponseRetries: Number.MAX_SAFE_INTEGER,
        onRetryAttempt: error => {
          const config = retryAxios.getConfig(error);
          debug(`Retry attempt #${config.currentRetryAttempt} at ${url}`);
        }
      }
    });
    debug(response.status, "requesting url", url);
    return Promise.resolve(response.data);
  } catch (exception) {
    debug("error", exception);
    return Promise.reject(exception);
  }
};

module.exports = createRequest;
