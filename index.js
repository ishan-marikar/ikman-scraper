const ikman = require("./lib/ikman");
const FileSystem = require("fs");

const query = "led monitor";

let x = new ikman({
  query: {
    searchTerm: query,
    region: "colombo",
    category: "electronics",
    language: "en"
  },
  crawler: {
    // maxNumberOfPages: 5
  }
});

// let generator = x._scraper();
// console.log(generator);
// for (const val of x._scraper()) {
//   console.log(val);
// }

// x.getListingsFromPage(
//   "https://ikman.lk/en/ads/colombo/electronics?query=tv"
// ).then(data => {
//   console.log(JSON.stringify(data, null, 2));
// });

x.on("data", data => {
  console.log("frontend got data from stream");
  console.log(JSON.stringify(data, null, 2));
});

x.on("end", () => {
  console.log("STREAM HAS ENDED");
});

x.scrape().then(data => {
  // console.log("data", data);
  console.log("numberOfItems", data.length);
  if (data.length > 0) {
    FileSystem.writeFileSync(
      `./to-delete/${query}-${Date.now()}.json`,
      JSON.stringify(data, null, 2)
    );
  }
});
